package application;

import java.awt.Checkbox;
import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;

import javax.naming.AuthenticationNotSupportedException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

public class addbatchcontroller implements Initializable {
	@FXML
	public ComboBox<String> addbatchcombo;
	@FXML
	public ComboBox<String> semestercombo;
	@FXML
	public Label batchstatus;
	@FXML
	public Label confirmlabel;
	@FXML
	public Button confirmyes;
	@FXML
	public Button confirmno;
	@FXML
	public Button finishbutton;

	ObservableList<String> batchlist = FXCollections.observableArrayList("1st Batch 027020xx", "2nd batch 037020xx",
			"3rd batch 047020xx", "4th batch 057020xx", "5th batch 067020xx", "6th batch 077020xx",
			"7th batch 087020xx", "8th batch 097020xx", "9th batch 107020xx", "10th batch 117020xx",
			"11th batch 127020xx", "12th batch 137020xx", "13th batch 147020xx", "14th batch 157020xx",
			"15th batch 167020xx", "16th batch 177020xx", "17th batch 187020xx", "18th batch 197020xx",
			"19th batch 207020xx", "20th batch 217020xx", "21th batch 227020xx", "22th batch 237020xx",
			"23th batch 247020xx", "24th batch 257020xx", "25th batch 267020xx", "26th batch 277020xx",
			"27th batch 287020xx", "28th batch 297020xx", "30th batch 317020xx");
	ObservableList<String> semesterlist = FXCollections.observableArrayList("1st semester", "2nd semester",
			"3rd semester", "4th semester", "5th semester", "6th semester", "7th semester", "8th semester", "MS 1st",
			"MS 2nd", "MS 3rd");
	Dbconnecter addbatchdatabase = new Dbconnecter();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		addbatchcombo.setItems(batchlist);
		addbatchcombo.getSelectionModel();
		semestercombo.setItems(semesterlist);
		semestercombo.setDisable(true);
		batchstatus.setText("Complete The Task Step By Step");
		confirmlabel.setVisible(false);
		confirmyes.setVisible(false);
		confirmno.setVisible(false);
		finishbutton.setVisible(false);

	}

	@FXML
	public void actionaddbatchcombo(ActionEvent event) {
		semestercombo.setDisable(false);
	}

	@FXML
	public void actionsave(ActionEvent event) {
		confirmlabel.setVisible(true);
		confirmyes.setVisible(true);
		confirmno.setVisible(true);
		// finishbutton.setVisible(true);
	}

	
	//By this action i first created a table for selected semester and batch,then i copied personal information(name,id,semester,batch,mobileno) of corresponding previous semester and then save them new table which i created previously.And Then i changed the promoted them in advance semester of which students information  i copied to new table.
	@FXML
	public void actionconfirmyes(ActionEvent event) {
		
		finishbutton.setVisible(true);
		String semester, batch, newsemester, newbatch, batchname, currentsemester, currentbatch;
		int integersemester ;
		try {
			newsemester = semestercombo.getValue();
			integersemester = Integer.parseInt(newsemester.substring(0, 1)) - 1;
			currentsemester = "" + integersemester;
			semester = newsemester.replaceAll(" ", "");
			newbatch = addbatchcombo.getValue();
			currentbatch = newbatch.substring(0, 3);
			batch = newbatch.replaceAll(" ", "");
			addbatchdatabase.createTableCub1(semester, batch);
			addbatchdatabase.studentsdatasearch(currentbatch, currentsemester, semester, batch);
			System.out.println("Current semester : " + currentsemester + " Current batch : " + currentbatch);
		} catch (Exception e) {
			e.printStackTrace();
		}
		addbatchdatabase.createTable2();

	}

	@FXML
	public void actionconfirmno(ActionEvent event) {

	}

	@FXML
	public void actionfinishbutton(ActionEvent event) {

	}

}
