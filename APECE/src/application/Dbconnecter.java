package application;

import java.awt.image.RasterFormatException;
import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sound.midi.VoiceStatus;

import com.mysql.jdbc.PreparedStatement;

public class Dbconnecter {
	public String o="UNKNOWN";
	public Connection con;
	public java.sql.Statement stat;
	public ResultSet rSet;
	public Dbconnecter(){
		try{
			Class.forName("com.mysql.jdbc.Driver");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3306/attendance","root", "");
			stat = con.createStatement();
		}catch(Exception ex){
			System.out.println("Error: "+ex);
		}
	}
	public void setCtData(){
		//String ct1, String ct2, String ct3, String ct4, String ct5, String ct6, String ct7, String ct8, String ct9, String ct10
		String id="15702015";
		String name="Towqir Ahmed Sharm";
		String semester="5th";
		String course="APEC 211";
		String year=o;
		String ct1=o;
		String ct2=o;
		String ct3=o;
		String ct4=o;
		String ct5=o;
		String ct6=o;
		String ct7=o;
		String ct8=o;
		String ct9=o;
		String ct10=o;
		String attendance=o;
		String dropout=o;
		String a1=o;
		String a2=o;
		String a3=o;
		String a4=o;
		String a5=o;
		String a6=o;
		String a7=o;
		String a8=o;

		try{
			String query="INSERT INTO 1stsemester (id, name, semester, course, year, ct1, ct2, ct3, ct4, ct5, ct6, ct7, ct8, ct9, ct10, attendance, dropout,  a1, a2, a3, a4, a5, a6, a7, a8)"+" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement preparedStatement=(PreparedStatement) con.prepareStatement(query);
			preparedStatement.setString(1, id);
			preparedStatement.setString(2, name);
			preparedStatement.setString(3, semester);
			preparedStatement.setString(4, course);
			preparedStatement.setString(5, year);
			preparedStatement.setString(6, ct1);
			preparedStatement.setString(7, ct2);
			preparedStatement.setString(8, ct3);
			preparedStatement.setString(9, ct4);
			preparedStatement.setString(10, ct5);
			preparedStatement.setString(11, ct6);
			preparedStatement.setString(12, ct7);
			preparedStatement.setString(13, ct8);
			preparedStatement.setString(14, ct9);
			preparedStatement.setString(15, ct10);
			preparedStatement.setString(16, attendance);
			preparedStatement.setString(17, dropout);
			preparedStatement.setString(18, a1);
			preparedStatement.setString(19, a2);
			preparedStatement.setString(20, a3);
			preparedStatement.setString(21, a4);
			preparedStatement.setString(22, a5);
			preparedStatement.setString(23, a6);
			preparedStatement.setString(24, a7);
			preparedStatement.setString(25, a8);
			
			preparedStatement.execute();
		}catch(Exception ex){
			System.out.println("Inserting Error : " +ex);
		}
	}
	public void setData(String nameset, String idset, String ageset, String mobilenumber){
	
		try{
			//String query="INSERT INTO towqir (name, id, age, mobilenumber)"+" VALUES (?,?,?,?)";
			String query="INSERT INTO course (course_name, course_code, course_credit, corresponding_syllabus)"+" VALUES (?,?,?,?)";
			PreparedStatement preparedStatement=(PreparedStatement) con.prepareStatement(query);
			
			preparedStatement.setString(1, nameset);
			preparedStatement.setString(2, idset);
			preparedStatement.setString(3, ageset);
			preparedStatement.setString(4, mobilenumber);
			preparedStatement.execute();
		}catch(Exception ex){
			System.out.println("Inserting Error : " +ex);
		}
	}
	public void replacedata(String initialquery){
		String a="15702056";
		String b="01521401124";
		String c="15702057";
		//UPDATE `towqir` SET `id` = REPLACE(`id`, '1570225', '15702015') WHERE `id` LIKE '%1570225%' COLLATE utf8mb4_bin
//		try {
//			String query="update towqir set id =replace(id, 15702051, 15702055) ";
//			stat.executeUpdate(query);
//		} catch (Exception e) {
//		
//		}
		try{
		//String query = "update towqir set id = "+c+" where mobilenumber like "+"'%"+b+"%'";
			String query=initialquery;
		PreparedStatement preparedStatement=(PreparedStatement) con.prepareStatement(query);
	     // preparedStatement.setString(1, a);
	      preparedStatement.execute();
		}catch(Exception e){
			System.out.println("Updating error : "+e);
			e.printStackTrace();
		}
		
	}
	public void search(){
		//SELECT * FROM `test` WHERE `id` = '22'
		try {
			String query="select * from towqir where "+"name like "+"'%ai%'";
			rSet=stat.executeQuery(query);
			System.out.println("After Searching : ");
			while(rSet.next()){
				String name= rSet.getString("name");
				String id=rSet.getString("id");
				String age  = rSet.getString("age");
				System.out.println("Name : "+name+"  ID : "+ id+" age : "+age);
			}
		} catch (Exception e) {
			System.out.println("Searching Error : "+ e);
		}
		
	}
	public void deleteData(){
		//DELETE FROM `test` WHERE 1
		try{
			String query="delete from towqir where "+ "id = "+"15702030";
			stat.executeUpdate(query);
			System.out.println("All items are deleted : ");
		}catch(Exception ex){
			System.out.println("After delecting all items : "+ex);
		}
	}
	
	public void getData(){
		try{
			String query = " select * from towqir";
			rSet=stat.executeQuery(query);
			System.out.println("Records From Database");
			while(rSet.next()){
				String name= rSet.getString("name");
				String id=rSet.getString("id");
				String age  = rSet.getString("age");
				String mobilenumber = rSet.getString("mobilenumber");
				System.out.println("Name : "+name+"  ID : "+ id+" age : "+age+" Mobile Number : "+mobilenumber);
				
			}
		}catch(Exception ex){
			System.out.println(ex);
		}
	}
	 public void createTableCub1(String semester, String batch) {  
	        try {
	        	 String sql = "CREATE TABLE " +semester +batch+ " "+
	                     "(id VARCHAR(11), " +
	                     " name VARCHAR(30), " + 
	                     " semester VARCHAR(11), " + 
	                     " course VARCHAR(11), " + 
	                     " year VARCHAR(11), " +
	                     " mobilenumber VARCHAR(20), " +
	                     " ct1 VARCHAR(11), " +
	                     " ct2 VARCHAR(11), " +
	                     " ct3 VARCHAR(11), " +
	                     " ct4 VARCHAR(11), " +
	                     " ct5 VARCHAR(11), " +
	                     " ct6 VARCHAR(11), " +
	                     " ct7 VARCHAR(11), " +
	                     " ct8 VARCHAR(11), " +
	                     " ct9 VARCHAR(11), " +
	                     " ct10 VARCHAR(11), " +
	                     " attendance VARCHAR(11), " +
	                     " dropout VARCHAR(11), " +
	                     " a2 VARCHAR(11), " +
	                     " a3 VARCHAR(11), " +
	                     " a4 VARCHAR(11), " +
	                     " a5 VARCHAR(11), " +
	                     " a6 VARCHAR(11), " +
	                     " a7 VARCHAR(11), " +
	                     " a8 VARCHAR(11) " +
	                     ")"; 

	        stat.executeUpdate(sql);
	        System.out.println("Created table in given database...");
	        }
	        catch(SQLException se){
	        	se.printStackTrace();
	        }
	        catch (Exception e) {
	            System.out.println("An error has occurred on Table Creation");
	        } 
	    }
	 public void createTable2() {  
	        try {
	        	 String sql = "CREATE TABLE studentsdata" + " "+
	                     "(id VARCHAR(11), " +
	                     " name VARCHAR(30), " + 
	                     " semester VARCHAR(11), " + 
	                     " batch VARCHAR(11), " +
	                     " address VARCHAR(11), " +
	                     " mobilenumber VARCHAR(11) " +
	                     ")"; 

	        stat.executeUpdate(sql);
	        System.out.println("Created table in given database...");
	        }
	        catch(SQLException se){
	        	se.printStackTrace();
	        }
	        catch (Exception e) {
	            System.out.println("An error has occurred on Table Creation");
	        }
	        
	    }
	 //This method is used in studentsdatasearch method 
	 public void setDataOfSemester(String Id, String Name, String Semester, String batch, String Mobilenumber, String semesterfullanme, String batchfullname){
			//String ct1, String ct2, String ct3, String ct4, String ct5, String ct6, String ct7, String ct8, String ct9, String ct10
			String id=Id;
			String name=Name;
			String semester=Semester;
			String course=o;
			String year=o;
			String ct1=o;
			String ct2=o;
			String ct3=o;
			String ct4=o;
			String ct5=o;
			String ct6=o;
			String ct7=o;
			String ct8=o;
			String ct9=o;
			String ct10=o;
			String attendance=o;
			String dropout=o;
			String mobilenumber=Mobilenumber;
			String a2=o;
			String a3=o;
			String a4=o;
			String a5=o;
			String a6=o;
			String a7=o;
			String a8=o;

			try{
				String query="INSERT INTO "+semesterfullanme+batchfullname+"(id, name, semester, course, year, mobilenumber, ct1, ct2, ct3, ct4, ct5, ct6, ct7, ct8, ct9, ct10, attendance, dropout,   a2, a3, a4, a5, a6, a7, a8)"+" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement preparedStatement=(PreparedStatement) con.prepareStatement(query);
				preparedStatement.setString(1, id);
				preparedStatement.setString(2, name);
				preparedStatement.setString(3, semester);
				preparedStatement.setString(4, course);
				preparedStatement.setString(5, year);
				preparedStatement.setString(6, ct1);
				preparedStatement.setString(7, ct2);
				preparedStatement.setString(8, ct3);
				preparedStatement.setString(9, ct4);
				preparedStatement.setString(10, ct5);
				preparedStatement.setString(11, ct6);
				preparedStatement.setString(12, ct7);
				preparedStatement.setString(13, ct8);
				preparedStatement.setString(14, ct9);
				preparedStatement.setString(15, ct10);
				preparedStatement.setString(16, attendance);
				preparedStatement.setString(17, dropout);
				preparedStatement.setString(18, mobilenumber);
				preparedStatement.setString(19, a2);
				preparedStatement.setString(20, a3);
				preparedStatement.setString(21, a4);
				preparedStatement.setString(22, a5);
				preparedStatement.setString(23, a6);
				preparedStatement.setString(24, a7);
				preparedStatement.setString(25, a8);
				
				preparedStatement.execute();
			}catch(Exception ex){
				System.out.println("Inserting Error : " +ex);
			}
		}
		public void studentsdatasearch(String batch, String semester, String semesterfullname, String batchfullname){
			//SELECT * FROM `test` WHERE `id` = '22'
			try {
				int integersemester;
				String currentsemester;
				String query="select * from studentsdata where "+"batch like "+"'%"+batch+"%' and semester like "+"'%"+semester+"%'";
				rSet=stat.executeQuery(query);
				System.out.println("After Searching : ");
				while(rSet.next()){
					String name= rSet.getString("name");
					String id=rSet.getString("id");
					String batchno  = rSet.getString("batch");
					String previoussemester = rSet.getString("semester");
					String mobilenumber = rSet.getString("mobilenumber");
				
					integersemester = Integer.parseInt(previoussemester.substring(0, 1)) + 1;
					currentsemester = "" + integersemester+"";
					setDataOfSemester(id, name, currentsemester, batchno, mobilenumber, semesterfullname, batchfullname);
					
					String studentsdataquery = "update studentsdata set semester = "+currentsemester+" where id like "+"'%"+id+"%'";
					replacedata(studentsdataquery);
					
					System.out.println("Name : "+name+"  ID : "+ id+" semster : "+previoussemester+" batch : "+batchno+" Mobilenumber : "+mobilenumber);
				}
			} catch (Exception e) {
				System.out.println("Searching Error : "+ e);
			}
			
		}
}
