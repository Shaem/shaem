package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			
			//Parent root=FXMLLoader.load(getClass().getResource("/application/apecexml.fxml"));
			Parent root =FXMLLoader.load(getClass().getResource("/application/apecexml.fxml"));
			Scene scene = new Scene(root,primaryStage.getMaxWidth(),primaryStage.getMaxHeight());
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setMaximized(true);
			primaryStage.setTitle("DMS-APECE");
			primaryStage.show();
			Dbconnecter dbconnectermain=new Dbconnecter();
			//dbconnectermain.createTableCub1();
			//dbconnectermain.replacedata();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
