package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class coursecontroller {
	@FXML Label courselabel;
	@FXML Button courseadd;
	@FXML Button finishbutton;
	@FXML TextField coursenametextfield;
	@FXML TextField coursecodetextfield;
	@FXML TextField coursecredittextfield;
	@FXML TextField coursesyallabustextfield;
	
	Dbconnecter courseconnector=new Dbconnecter();
	String coursename, coursecode, coursecredit, coursesyallabus;
	
	@FXML public void courseaddbutton(ActionEvent event){
		courselabel.setText("New Window");
		coursename=coursenametextfield.getText();
		coursecode=coursecodetextfield.getText();
		coursecredit=coursecredittextfield.getText();
		coursesyallabus=coursesyallabustextfield.getText();
		try{
			courseconnector.setData(coursename, coursecode, coursecredit, coursesyallabus);
			courselabel.setText("Successfull");
		}catch(Exception e){
			System.out.println("Something Wrong");
		}
		
	}
	
	
	@FXML public void coursefinishbutton(ActionEvent event) throws Exception{
		Stage stage = (Stage) finishbutton.getScene().getWindow();
	    stage.close();
}

}
