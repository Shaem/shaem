package application;

import java.net.URL;
import java.util.Observable;
import java.util.ResourceBundle;

import javax.sound.midi.VoiceStatus;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class ctmarksxmlcontroller implements Initializable{
	
	@FXML
	public ComboBox<String> ctmarkssemestercombo;
	@FXML
	public ComboBox<String> ctmarkscoursecombo;
	@FXML
	public ComboBox<String> ctmarksidcombo;
	@FXML
	public ComboBox<String> secondidcombo;
	@FXML 
	public ComboBox<String> ctmarksnumbercombo;
	@FXML
	public ComboBox<String> ctmarkscalculatecombo;
	@FXML
	public ComboBox<String> ctmarkstopcountcombo;
	@FXML public TextField ct1label;
	@FXML public TextField ct2label;
	@FXML public TextField ct3label;
	@FXML public TextField ct4label;
	@FXML public TextField ct5label;
	@FXML public TextField ct6label;
	@FXML public TextField ct7label;
	@FXML public TextField ct8label;
	@FXML public TextField ct9label;
	@FXML public TextField ct10label;
	
	@FXML public Label topbestcountlabel;
	@FXML
	public TextField ctmarksidtextfield;
	@FXML 
	public Label ctmarksstatuslabel;
	
	ObservableList<String> list=FXCollections.observableArrayList("1st Semester","2nd Semester", "3rd Semester", "4th Semester","5th Semester");
	ObservableList<String> courselist=FXCollections.observableArrayList("APEC-212","APEC-312", "APEC-412", "APEC-512","APEC-612");
	ObservableList<String> idlist=FXCollections.observableArrayList("157020","147020", "137020", "127020","117020", "167020","177020");
	ObservableList<String> ctmarks=FXCollections.observableArrayList("20","15", "10", "18","12", "25","30");
	ObservableList<String> ctcount=FXCollections.observableArrayList("20","15", "10", "18","12", "25","30");
	ObservableList<String> topctcount=FXCollections.observableArrayList("1","2", "3", "4","5", "6","7","8","9","10");
	ObservableList<String> id=FXCollections.observableArrayList("01","02", "03", "04","05", "06","07","08","09","10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20");
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		ctmarkssemestercombo.setItems(list);
		ctmarkssemestercombo.getSelectionModel();
		ctmarkscoursecombo.setItems(courselist);
		ctmarksnumbercombo.setItems(ctmarks);
		ctmarkscalculatecombo.setItems(ctcount);
		ctmarkstopcountcombo.setItems(topctcount);
		ctmarksidcombo.setItems(idlist);
		secondidcombo.setItems(id);
		secondidcombo.setDisable(true);
	}
	Dbconnecter ctdbconnector=new Dbconnecter();
	String ctquery, marks1, marks2, marks3, marks4, marks5, marks6, marks7, marks8, marks9, marks10;
	@FXML
	public void actionctmarksidcombo(ActionEvent event){
		secondidcombo.setDisable(false);
	}
	@FXML
	public void actionsecondidcombo(ActionEvent event) {
		String idstatus="";
		int selectedid=Integer.parseInt(ctmarksidcombo.getValue());
		int selectedsecondid=Integer.parseInt(secondidcombo.getValue());
		idstatus=selectedid+""+selectedsecondid;
		ctmarksstatuslabel.setText("You Selected : "+ idstatus);
	}
	@FXML
	public void actionsave(ActionEvent event){
		System.out.println(ctmarkssemestercombo.getValue());
		double ctmarks[]=new double[11];
		for(int i=1;i<=10;i++)
			ctmarks[i]=0;
		int count=0;
		int wrongasnwer = 0;
		ctmarkstopcountcombo.setEditable(true);
		//topbestcountlabel.setVisible(false);
		//ct1label.setDisable(true);
		int ctcalculatemarks=0,ctselectedmarks=0;
		double result,sum=0.0;
		
			try{
				ctselectedmarks=Integer.parseUnsignedInt(ctmarksnumbercombo.getValue());
				ctcalculatemarks=Integer.parseInt(ctmarkscalculatecombo.getValue());
				marks1=ct1label.getText();
				if(marks1!=null){
					ctmarks[1]=Double.parseDouble(marks1);
					if(ctselectedmarks<ctmarks[1])
						wrongasnwer=1; 
					System.out.println("CT marks 1: "+ctmarks[1]);
					System.out.println("Sum 1 :"+sum + " Count : "+count);
					}
			    marks2=ct2label.getText();
				if(marks2!=null){
					ctmarks[2]=Double.parseDouble(marks2);
					if(ctselectedmarks<ctmarks[2])
						wrongasnwer=1; 
					System.out.println("CT marks 2: "+ctmarks[2]);
					System.out.println("Sum 2 :"+sum+" Count : "+count);
					}
				marks3=ct3label.getText();
				System.out.println("label 3 :"+marks3);
				if(marks3!=null){
					ctmarks[3]=Double.parseDouble(marks3);
					if(ctselectedmarks<ctmarks[3])
						wrongasnwer=1; 
					System.out.println("CT marks 3: "+ctmarks[3]);
					System.out.println("Sum 3 :"+sum+" Count : "+count);
					}
				marks4=ct4label.getText();
				if(marks4!=null){
					ctmarks[4]=Double.parseDouble(marks4);
					if(ctselectedmarks<ctmarks[4])
						wrongasnwer=1; 
					System.out.println("CT marks 4: "+ctmarks[4]);
					System.out.println("Sum 4 :"+sum+" Count : "+count);
					}
				marks5=ct5label.getText();
				if(marks5!=null){
					ctmarks[5]=Double.parseDouble(marks5);
					if(ctselectedmarks<ctmarks[5])
						wrongasnwer=1; 
					System.out.println("CT marks 5: "+ctmarks[5]);
					System.out.println("Sum 5 :"+sum+" Count : "+count);
					}
				marks6=ct6label.getText();
				if(marks6!=null){
					ctmarks[6]=Double.parseDouble(marks6);
					if(ctselectedmarks<ctmarks[6])
						wrongasnwer=1; 
					System.out.println("CT marks 6: "+ctmarks[6]);
					System.out.println("Sum 6 :"+sum+" Count : "+count);
					}
				marks7=ct7label.getText();
				if(marks7!=null){
					ctmarks[7]=Double.parseDouble(marks7);
					if(ctselectedmarks<ctmarks[7])
						wrongasnwer=1; 
					System.out.println("CT marks 7: "+ctmarks[7]);
					System.out.println("Sum 7 :"+sum+" Count : "+count);
					}
				marks8=ct8label.getText();
				if(marks8!=null){
					ctmarks[8]=Double.parseDouble(marks8);
					if(ctselectedmarks<ctmarks[8])
						wrongasnwer=1; 
					System.out.println("CT marks 8: "+ctmarks[8]);
					System.out.println("Sum 8 :"+sum+" Count : "+count);
					}
				marks9=ct9label.getText();
				if(marks9!=null){
					ctmarks[9]=Double.parseDouble(marks9);
					if(ctselectedmarks<ctmarks[9])
						wrongasnwer=1; 
					System.out.println("CT marks 9: "+ctmarks[9]);
					System.out.println("Sum 9 :"+sum+" Count : "+count);
					}
				marks10=ct10label.getText();
				if(marks10!=null){
					ctmarks[10]=Double.parseDouble(marks10);
					if(ctselectedmarks<ctmarks[10])
						wrongasnwer=1; 
					System.out.println("CT marks 10: "+ctmarks[10]);
					System.out.println("Sum 10 :"+sum+" Count : "+count);
					}	 
				
			}catch(Exception e){
				System.out.println("Your Input Is Wrong");
				//e.printStackTrace();
			}
			if(wrongasnwer==1){
				ctmarksstatuslabel.setText("Your Input Number is greater than Your Selected Number");
			}
			
			ctquery = "update 1stsemester set ct1 = "+marks1+", ct2 = "+marks2+", ct3 = "+marks3+", ct4 = "+marks4+", ct5 = "+marks5+", ct6 = "+marks6+", ct7 = "+marks7+", ct8 = "+marks8+", ct9 = "+marks9+", ct10 = "+marks10+" where id like "+"'%15702015%'";
			ctdbconnector.replacedata(ctquery);
			
			count=Integer.parseUnsignedInt(ctmarkstopcountcombo.getValue());
			double x;
		    for(int i = 1; i <= 10; i++)
		        for(int j = i+1; j <= 10; j++) {
		        	if(ctmarks[i] > ctmarks[j]) {
		        		x = ctmarks[i];
		            ctmarks[i] = ctmarks[j];
		            ctmarks[j] = x;
		        }
		    }
		    for(int i=10; i > (10-count); i--){
		    	System.out.println("After Sorting Ct marks : "+ctmarks[i]);
		    	ctmarks[i]= (ctmarks[i]*ctcalculatemarks) / ctselectedmarks;
		    	sum+=ctmarks[i];
		    	System.out.println("Sum : "+sum);
		    }

		
		System.out.println("Count : "+count);
		result=sum/count;
		System.out.println("The Average Result : "+result);
		String labeltext= ctmarkssemestercombo.getValue() + " " + ctmarkscoursecombo.getValue()+ " " + ctmarksidcombo.getValue()+ctmarksidtextfield.getText()+" "+ctmarksnumbercombo.getValue()+" "+ctmarkscalculatecombo.getValue()+" "+ctmarkstopcountcombo.getValue();
		ctmarksstatuslabel.setText(labeltext);
	}
	
}
