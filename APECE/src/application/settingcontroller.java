package application;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class settingcontroller {
	@FXML public Label settinglabel;
	
	public Stage primarystage2=new Stage();
	public void addcoursebutton(ActionEvent event) throws Exception{
		
			settinglabel.setText("Login Successfull");			
			Parent root=FXMLLoader.load(getClass().getResource("/application/course.fxml"));
			Scene scene = new Scene(root,500,500);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primarystage2.setScene(scene);
			primarystage2.show();

	}
	
	public Stage primarystage3=new Stage();
	public void actionaddbatch(ActionEvent event) throws Exception{
		settinglabel.setText("You recently work on a batch");
		Parent root1=FXMLLoader.load(getClass().getResource("/application/addbatchxml.fxml"));
		Scene scene1=new Scene(root1, 300, 300);
		scene1.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primarystage3.setScene(scene1);
		primarystage3.show();
	}

}
