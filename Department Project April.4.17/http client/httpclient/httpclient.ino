/*
   http client...
   Developer : Towqir Ahmed Shaem
   Date : April.4.2017
*/
#define sclk 14
#define mosi 13
#define cs   15
#define dc   2
#define rst  16
#define switchOut  4
#define switchIn  5
int idNumber;
int c;
int i;
int number[60];
int result;

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;
int templatenumber;
String var;

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&Serial);
Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, rst);

void setup() {
  USE_SERIAL.begin(115200);
  for (uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.flush();
    delay(1000);
  }

  finger.begin((uint64_t)115200);
  tft.initR(INITR_BLACKTAB);
  pinMode(switchOut, OUTPUT);
  pinMode(switchIn, INPUT);
  finger.ToggleLed(true);
  tft.fillScreen(ST7735_BLUE);
  tft.setCursor(10, 10);
  tft.setTextSize(2);
  tft.setTextColor(ST7735_WHITE);
  // ****************Graphics Design******************** //
  tft.drawTriangle(64, 100, 64, 150, 100, 150, ST7735_YELLOW);
  tft.drawTriangle(64, 100, 44, 150, 84, 150,  ST7735_RED);
  delay(1000);
  tft.fillTriangle(64, 100, 64, 150, 100, 150, ST7735_YELLOW);
  tft.fillTriangle(64, 100, 44, 150, 84, 150,  ST7735_RED);
  // **************************************************** //
  delay(3000);
  tft.println("WELCOME!!");
  tft.println("");
  tft.setTextSize(3);
  tft.print(" APECE");
  for (int i = 0; i < 2; i++)
  {
    tft.invertDisplay(true);
    delay(500);
    tft.invertDisplay(false);
    delay(500);
  }
  //WiFiMulti.addAP("STELLAR", "stellarbd");
  WiFiMulti.addAP("Shaem @ Futurenet Online", "ajmstt22");
  //WiFiMulti.addAP("Redmi 2", "d6lordlx");
  //WiFiMulti.addAP("Roboto Sans", "chemistry");
}

void loop() {
  digitalWrite(switchOut, HIGH);
  delay(100);
  c = digitalRead(switchIn);
  if (c == 1) {
    tft.fillScreen(ST7735_BLUE);
    tft.setCursor(5, 10);
    tft.setTextSize(1);
    tft.setTextColor(ST7735_WHITE);
    tft.print(" ENROLL PLEASE!!");
    delay(2000);
    tft.fillScreen(ST7735_BLUE);
    result = finger.Enroll_into_memory();
    if (result == 0)
    {
      tft.setCursor(5, 40);
      tft.print("Previously");
      tft.print(" Enrolled");
      tft.println("");
      tft.setCursor(50, 50);
      tft.println("or");
      tft.setCursor(40, 60);
      tft.print("Error!!!");
      delay(3000);
      tft.fillScreen(ST7735_BLUE);
    }
    else {
      tft.fillScreen(ST7735_BLUE);
      tft.setCursor(0, 10);
      tft.setTextSize(2);
      tft.setTextColor(ST7735_WHITE);
      tft.println("ENROLLED!!");
      for (int i = 0; i < 2; i++)
      {
        tft.invertDisplay(true);
        delay(500);
        tft.invertDisplay(false);
        delay(500);
      }
      tft.fillScreen(ST7735_BLUE);
      tft.setCursor(0, 10);
      tft.setTextSize(2);
      tft.setTextColor(ST7735_WHITE);
      tft.print("ID:");
      tft.print(result);
      delay(3000);
      tft.fillScreen(ST7735_BLUE);
    }
  }

  digitalWrite(switchOut, LOW);
  delay(100);
  c = 0;
  tft.fillScreen(ST7735_BLUE);
  tft.setTextSize(1);
  tft.setCursor(7, 50);
  tft.setTextColor(ST7735_WHITE);
  tft.print("Identify please!!");
  delay(3000);
  //tft.fillScreen(ST7735_BLUE);

  /* ** Identify Start ** */
  idNumber = finger.Identify();
  if (idNumber > 0) {
    for (i = 1; i < 60; i++) {
      if (number[i] == idNumber) {  // Checking it whether it is already identified or not.
        idNumber = 9600;
        tft.fillScreen(ST7735_BLUE);
        tft.setTextSize(1);
        tft.setCursor(5, 50);
        // tft.fillScreen(ST7735_BLUE);
        tft.setTextColor(ST7735_WHITE);
        tft.println("Already Identified");
        delay(2000);
        break;
      }
      if (number[i] == 0) {  // If it is not identified then assigning it another variable;
        number[i] = idNumber;
        break;
      }
    }
  }
  if (idNumber > 0 && idNumber != 9600 ) {
    tft.fillScreen(ST7735_BLUE);
    tft.setTextSize(1);
    tft.setCursor(20, 50);
    tft.setTextColor(ST7735_WHITE);
    tft.println("IDENTIFIED!!");
    // tft.println(idNumber);
    for (int i = 0; i < 2; i++)
    {
      tft.invertDisplay(true);
      delay(500);
      tft.invertDisplay(false);
      delay(500);
    }
    tft.fillScreen(ST7735_BLUE);
    if ((WiFiMulti.run() == WL_CONNECTED)) {
      HTTPClient http;
      var = String(idNumber);
      http.begin("http://www000128com.000webhostapp.com/?var=" + var);
      int httpCode = http.GET();
      if (httpCode > 0) {
        if (httpCode == HTTP_CODE_OK) {
          String payload = http.getString();
        }
      }
      http.end();

      http.begin("http://apececu.000webhostapp.com/?var=" + var);
      int httpCode2 = http.GET();
      if (httpCode2 > 0) {
        if (httpCode2 == HTTP_CODE_OK) {
          String payload2 = http.getString();
        }
      }
      http.end();

      http.begin("http://apececu2.000webhostapp.com/?var=" + var);
      int httpCode3 = http.GET();
      if (httpCode3 > 0) {
        if (httpCode == HTTP_CODE_OK) {
          String payload3 = http.getString();
        }
      }
      http.end();
    }
  }
  else if (idNumber <= -1) {
    if (idNumber == -1) {
      //  tft.fillScreen(ST7735_BLUE);
      //    tft.setTextSize(3);
      //    tft.setCursor(0, 10);
      //    tft.setTextColor(ST7735_WHITE);
      //    for (int i = 0; i < 2; i++)
      //    {
      //      tft.invertDisplay(true);
      //      delay(500);
      //      tft.invertDisplay(false);
      //      delay(500);
      //    }
      //    tft.setTextSize(1);
      //    tft.setCursor(10, 50);
      //    tft.print("  Time Out!!");
      //    delay(2000);

    }
    if (idNumber == -3) {
      tft.fillScreen(ST7735_BLUE);
      tft.setTextSize(1);
      tft.setCursor(10, 50);
      tft.setTextColor(ST7735_WHITE);
      tft.println("Didn't Match!!");
      for (int i = 0; i < 2; i++)
      {
        tft.invertDisplay(true);
        delay(500);
        tft.invertDisplay(false);
        delay(500);
      }
      tft.setTextSize(1);
      tft.setCursor(10, 90);
      tft.print("Enroll first!!");
      delay(2000);
    }
    if (idNumber == -2) {
      tft.fillScreen(ST7735_BLUE);
      //tft.setTextSize(3);
      //tft.setCursor(0, 30);
      tft.setTextColor(ST7735_WHITE);
      //tft.println("Error!!");

      tft.setTextSize(1);
      tft.setCursor(10, 90);
      tft.print("Enroll first!!");
      for (int i = 0; i < 2; i++)
      {
        tft.invertDisplay(true);
        delay(500);
        tft.invertDisplay(false);
        delay(500);
      }
      //delay(2000);

    }
  }
  c = 0;

}
