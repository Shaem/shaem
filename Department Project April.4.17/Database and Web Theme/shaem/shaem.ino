#define sclk 14
#define mosi 13
#define cs   15
#define dc   2
#define rst  16
#define switchOut  4
#define switchIn  5
int templateid;
int c;
int i;
int number[60] = {0};
int result;
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#define USE_SERIAL Serial

ESP8266WiFiMulti WiFiMulti;
int templatenumber;
String var;

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library
#include <SPI.h>
#include <Adafruit_Fingerprint.h>
#include <SoftwareSerial.h>

Adafruit_Fingerprint finger = Adafruit_Fingerprint(&Serial);
Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, rst);
//Adafruit_ST7735 tft = Adafruit_ST7735(cs, dc, mosi, sclk, rst);

void enroll(int x) {
  if (x == 0) {
    tft.fillScreen(ST7735_BLACK);
    tft.setCursor(5, 10);
    tft.setTextSize(1);
    tft.setTextColor(ST7735_WHITE);
    tft.print("Ready For Enroll!!");
    delay(800);
    tft.fillScreen(ST7735_BLACK);
    result = finger.Enroll_into_memory();
    if (result == 0)
    {
      tft.setCursor(0, 10);
      tft.println("  Previously");
      tft.print("Enrolled or Error");
      c = digitalRead(switchIn);
      if (c == 0)
        enroll(c);
      delay(800);
      tft.fillScreen(ST7735_BLACK);
    }
    else if (result > 0) {
      tft.println("ENROLLED!!");
      delay(1000);
      tft.setCursor(20, 50);
      tft.print("Your Template :  ");
      tft.print(result);
      delay(1000);
      tft.fillScreen(ST7735_BLACK);
    }
  }
}
void identifycheck(int idNumber) {
  if (idNumber > 0) {
    c = digitalRead(switchIn);
    if (c == 0)
      enroll(c);
    tft.fillScreen(ST7735_BLACK);
    tft.setTextSize(1);
    tft.setCursor(20, 50);
    tft.setTextColor(ST7735_WHITE);
    tft.println("IDENTIFIED!!");
    tft.print("    ");
    tft.println(idNumber);
    delay(800);
    c = digitalRead(switchIn);
    if (c == 0)
      enroll(c);
    tft.fillScreen(ST7735_BLACK);
  }
  else if (idNumber == 0) {
    c = digitalRead(switchIn);
    if (c == 0)
      enroll(c);
    tft.fillScreen(ST7735_BLACK);
    tft.setTextSize(3);
    tft.setCursor(0, 10);
    tft.setTextColor(ST7735_WHITE);
    tft.println("Error!!");
    tft.setTextSize(1);
    tft.setCursor(10, 50);
    tft.println("Do Registration!!");
    tft.print("Press the Switch");
    delay(800);
    c = digitalRead(switchIn);
    if (c == 0)
      enroll(c);
    tft.fillScreen(ST7735_BLACK);
  }
}
void setup() {
  USE_SERIAL.begin(115200);
  USE_SERIAL.println();
  USE_SERIAL.println();
  USE_SERIAL.println();

  for (uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(1000);
  }

  WiFiMulti.addAP("STELLAR", "stellarbd");
  finger.begin((uint64_t)115200);
  tft.initR(INITR_BLACKTAB);
  pinMode(switchOut, OUTPUT);
  digitalWrite(switchOut, HIGH);
  pinMode(switchIn, INPUT);
  Serial.begin(115200);
  finger.ToggleLed(true);
}

void loop() {
  if ((WiFiMulti.run() == WL_CONNECTED)) {
    templateid = finger.Identify();
    identifycheck(templateid);
    c = digitalRead(switchIn);
    if (c == 0)
      enroll(c);
    HTTPClient http;
    USE_SERIAL.print("[HTTP] begin...\n");
    var = String(templatenumber);

    for (i = 1; i < 60; i++) {
      if (number[i] == templateid) {
        templateid = 0;
        break;
      }
      if (number[i] == 0) {
        number[i] = templateid;
        break;
      }
    }
    if (templateid > 0) {
      var = String(templateid);
      http.begin("http://www000128com.000webhostapp.com/?var=" + var);
      int httpCode = http.GET();
      if (httpCode > 0) {
        if (httpCode == HTTP_CODE_OK) {
          String payload = http.getString();
        }
      }
      http.end();

      http.begin("http://apececu.000webhostapp.com/?var=" + var);
      int httpCode2 = http.GET();
      if (httpCode2 > 0) {
        if (httpCode2 == HTTP_CODE_OK) {
          String payload2 = http.getString();
        }
      }
      http.end();

      http.begin("http://apececu2.000webhostapp.com/?var=" + var);
      int httpCode3 = http.GET();
      if (httpCode3 > 0) {
        if (httpCode == HTTP_CODE_OK) {
          String payload3 = http.getString();
        }
      }
      http.end();
    }
  }
}
